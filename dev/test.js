
const Blockchain = require('./blockchain');

const bitcoin = new Blockchain();

const btc1 = {
    "chain": [
      {
        "index": 1,
        "timestamp": 1613304928164,
        "transactions": [],
        "nonce": 0,
        "hash": "0",
        "previousBlockHash": "0"
      },
      {
        "index": 2,
        "timestamp": 1613304950050,
        "transactions": [],
        "nonce": 18140,
        "hash": "0000b9135b054d1131392c9eb9d03b0111d4b516824a03c35639e12858912100",
        "previousBlockHash": "0"
      },
      {
        "index": 3,
        "timestamp": 1613304950617,
        "transactions": [],
        "nonce": 92894,
        "hash": "00002778916a7dadc7260a1b6cff17be291bda44445d157e48da55fe9dbb06b3",
        "previousBlockHash": "0000b9135b054d1131392c9eb9d03b0111d4b516824a03c35639e12858912100"
      },
      {
        "index": 4,
        "timestamp": 1613305038057,
        "transactions": [
          {
            "amount": 6,
            "sender": "00",
            "recipient": "5314b6406ebe11ebb495758452949064",
            "transactionId": "6078e7706ebe11ebb495758452949064"
          },
          {
            "amount": 6,
            "sender": "00",
            "recipient": "5314b6406ebe11ebb495758452949064",
            "transactionId": "607a1ff06ebe11ebb495758452949064"
          },
          {
            "amount": 200,
            "sender": "DUDIWOOUCNUFDH",
            "recipient": "DIUDHUHFILKOOJMO",
            "transactionId": "84f3e4b06ebe11ebb495758452949064"
          },
          {
            "amount": 2009,
            "sender": "DUDIWOOUCNUFDH",
            "recipient": "DIUDHUHFILKOOJMO",
            "transactionId": "87dbe9706ebe11ebb495758452949064"
          },
          {
            "amount": 2,
            "sender": "DUDIWOOUCNUFDH",
            "recipient": "DIUDHUHFILKOOJMO",
            "transactionId": "8c8685c06ebe11ebb495758452949064"
          }
        ],
        "nonce": 298579,
        "hash": "00006f68811cfb2209ec5a9f86ca5169a9b8ff0f01d6874b0049b2c22a4480c5",
        "previousBlockHash": "00002778916a7dadc7260a1b6cff17be291bda44445d157e48da55fe9dbb06b3"
      },
      {
        "index": 5,
        "timestamp": 1613305100878,
        "transactions": [
          {
            "amount": 6,
            "sender": "00",
            "recipient": "5314b6406ebe11ebb495758452949064",
            "transactionId": "9495a9d06ebe11ebb495758452949064"
          },
          {
            "amount": 40,
            "sender": "DUDIWOOUCNUFDH",
            "recipient": "DIUDHUHFILKOOJMO",
            "transactionId": "ae9b58706ebe11ebb495758452949064"
          },
          {
            "amount": 60,
            "sender": "DUDIWOOUCNUFDH",
            "recipient": "DIUDHUHFILKOOJMO",
            "transactionId": "b18495b06ebe11ebb495758452949064"
          },
          {
            "amount": 70,
            "sender": "DUDIWOOUCNUFDH",
            "recipient": "DIUDHUHFILKOOJMO",
            "transactionId": "b5b287f06ebe11ebb495758452949064"
          }
        ],
        "nonce": 999,
        "hash": "000077fb8fb0601fa80f4ea053e127d8405c31261d513a02884da3a061676fc8",
        "previousBlockHash": "00006f68811cfb2209ec5a9f86ca5169a9b8ff0f01d6874b0049b2c22a4480c5"
      },
      {
        "index": 6,
        "timestamp": 1613305104058,
        "transactions": [
          {
            "amount": 6,
            "sender": "00",
            "recipient": "5314b6406ebe11ebb495758452949064",
            "transactionId": "ba073c106ebe11ebb495758452949064"
          }
        ],
        "nonce": 41011,
        "hash": "0000267e53c110fc650d70729e2f8d1eeab22929effaad710c32eb43c600b00e",
        "previousBlockHash": "000077fb8fb0601fa80f4ea053e127d8405c31261d513a02884da3a061676fc8"
      }
    ],
    "pendingTransactions": [
      {
        "amount": 6,
        "sender": "00",
        "recipient": "5314b6406ebe11ebb495758452949064",
        "transactionId": "bbec4fc06ebe11ebb495758452949064"
      }
    ],
    "currentNodeUrl": "http://localhost:3001",
    "networkNodes": []
  };
    


console.log(bitcoin.chainIsValid(btc1.chain));
