# Create blockchain in javascript #

In this project i create a blockchain from scratch in javascript. The blockchain run locally unto 5 nodes.
I also build an UI to query the blockchain.

### how do that work? ###

*  Starts the programm on each node with the command 'npm run node_X'. Where 1 <= X => 5.
*  Create a network with the different nodes. To do that we need to register and broadcast each node.
Therefore i implemented the '/register-and-broadcast-node' endpoint.
*  With the '/transaction/broadcast' endpoint we can create and broadcast new transaction to the network.
*  With the '/mine' endpoint we can mine a block. The block will then be added to the blockchain.
*  With the '/consensus' endpoint we provide a new network member the correct blockchain data.

### Which library? ###

* express.js to build the api.
* body-parser.js to automatically parse http request body as Json object.
* request-promises to make http request.
* uuid.js to create an unique identifier.

